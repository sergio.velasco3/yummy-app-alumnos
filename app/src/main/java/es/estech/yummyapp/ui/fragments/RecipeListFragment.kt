package es.estech.yummyapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.estech.yummyapp.R
import es.estech.yummyapp.data.models.recipes.Recipe
import es.estech.yummyapp.databinding.FragmentAnyListBinding
import es.estech.yummyapp.ui.MainActivity
import es.estech.yummyapp.ui.MyViewModel
import es.estech.yummyapp.ui.adapters.AdapterRecipes

class RecipeListFragment : Fragment() {

    private var _binding: FragmentAnyListBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: AdapterRecipes

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentAnyListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configRecycler()

        //todo
        // llama a la función para obtener el tag seleccionado en la pantalla anterior
        // cuando lo tengas, llama a la función para obtener las recetas de ese tag
//        myViewModel....observe {
              // cambia título toolbar
//            (requireActivity() as MainActivity).supportActionBar?.title = it
            //todo: con el tag seleccionado, haz la llamada a la api para obtener la lista de recetas
//            myViewModel...observe { response ->
//
//                  //todo inserta lista recetas en adaptador
//                    adapter.updateList(list)
//
//            }
//        }
    }

    /**
     * inicializa el recyclerview
     */
    private fun configRecycler() {
        val recyclerView = binding.recyclerview
        adapter = AdapterRecipes(object : AdapterRecipes.RecipeClick {
            override fun onRecipeClick(recipe: Recipe) {
                // todo click sobre receta en listado
                findNavController().navigate(R.id.action_recipeListFragment_to_recipeDetailFragment)
            }
        })
        val layoutManager = GridLayoutManager(requireContext(), 2)

        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                RecyclerView.VERTICAL
            )
        )

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}