package es.estech.yummyapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import es.estech.yummyapp.data.models.recipes.Recipe
import es.estech.yummyapp.databinding.HolderTagBinding

class AdapterTags(
    val listener: TagClick
) : RecyclerView.Adapter<AdapterTags.TagHolder>() {

    /**
     * interface con función de click en tag
     */
    interface TagClick {
        fun onTagClick(tag: String)
    }

    private val list = ArrayList<String>()

    inner class TagHolder(val binding: HolderTagBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TagHolder(HolderTagBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TagHolder, position: Int) {
        val tag = list[position]
        with(holder.binding) {
            tvTagTitle.text = tag
        }
        holder.itemView.setOnClickListener {
            listener.onTagClick(tag)
        }
    }

    fun updateList(newList: List<String>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }
}