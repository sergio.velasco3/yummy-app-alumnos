package es.estech.yummyapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import es.estech.yummyapp.data.models.comments.Comment
import es.estech.yummyapp.data.models.recipes.Recipe
import es.estech.yummyapp.databinding.HolderCommentBinding

class AdapterComments(
    val listener: CommentClick
) : RecyclerView.Adapter<AdapterComments.CommentHolder>() {

    /**
     * Interface con funciones para click en botón editar y en botón eliminar
     */
    interface CommentClick {
        fun onEditClick(comment: Comment)
        fun onDeleteClick(comment: Comment)
    }

    private val list = ArrayList<Comment?>()

    inner class CommentHolder(val binding: HolderCommentBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentHolder {
        val inflater = LayoutInflater.from(parent.context)
        return CommentHolder(HolderCommentBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CommentHolder, position: Int) {
        val comment = list[position]
        with(holder.binding) {

            if (comment != null) {
                tvBody.text = comment.body
                tvRecipeId.text = comment.postId.toString()
                tvUserId.text = comment.user?.id.toString()

                // click eliminar
                ivDelete.setOnClickListener {
                    listener.onDeleteClick(comment)
                }

                // click editar
                ivEdit.setOnClickListener {
                    listener.onEditClick(comment)
                }
            }

        }
    }

    fun updateList(newList: List<Comment?>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }
}