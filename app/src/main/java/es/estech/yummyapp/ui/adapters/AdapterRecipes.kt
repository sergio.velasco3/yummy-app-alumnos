package es.estech.yummyapp.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import es.estech.yummyapp.data.models.recipes.Recipe
import es.estech.yummyapp.databinding.HolderRecipeBinding

class AdapterRecipes(
    val listener: RecipeClick
) : RecyclerView.Adapter<AdapterRecipes.RecipeHolder>() {

    /**
     * interface para función de click en receta
     */
    interface RecipeClick {
        fun onRecipeClick(recipe: Recipe)
    }

    private val list = ArrayList<Recipe?>()

    inner class RecipeHolder(val binding: HolderRecipeBinding) : ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RecipeHolder(HolderRecipeBinding.inflate(inflater, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: RecipeHolder, position: Int) {
        val recipe = list[position]
        with(holder.binding) {
            tvTitle.text = recipe?.name
            tvDiff.text = recipe?.difficulty
            recipe?.rating?.let {
                ratingBar.rating = it.toFloat()
                tvRating.text = it.toString()
            }
            Glide.with(holder.itemView).load(recipe?.image).into(imageView)

            // click en celda
            holder.itemView.setOnClickListener{
                if (recipe != null) {
                    listener.onRecipeClick(recipe)
                }
            }
        }
    }

    fun updateList(newList: List<Recipe?>) {
        list.clear()
        list.addAll(newList)
        notifyDataSetChanged()
    }
}