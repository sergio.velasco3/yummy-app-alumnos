package es.estech.yummyapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import es.estech.yummyapp.R
import es.estech.yummyapp.databinding.FragmentAnyListBinding
import es.estech.yummyapp.ui.MainActivity
import es.estech.yummyapp.ui.MyViewModel
import es.estech.yummyapp.ui.adapters.AdapterTags

class TagListFragment : Fragment() {

    private var _binding: FragmentAnyListBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: AdapterTags

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentAnyListBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        configRecycler()
        addMenu()

        //TODO llama a la función para obtener la lista de tags
        // en el observe, llama a adapter.updateList() y pásale el listado de tags
//        myViewModel...observe {
//                adapter.updateList(it)
//
//        }

    }

    /**
     * configura el recyclerview
     */
    private fun configRecycler() {
        val recyclerView = binding.recyclerview
        adapter = AdapterTags(object : AdapterTags.TagClick {

            override fun onTagClick(tag: String) {
                // TODO click sobre uno de los tags
                findNavController().navigate(R.id.action_tagListFragment_to_recipeListFragment)
            }
        })
        val layoutManager = LinearLayoutManager(requireContext())
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                RecyclerView.VERTICAL
            )
        )
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    /**
     * añade menú superior con botón comentarios
     */
    private fun addMenu() {
        (requireActivity() as MainActivity).addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.menu_main, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                if (menuItem.itemId == R.id.action_comments) {
                    findNavController().navigate(R.id.action_tagListFragment_to_commentsFragment)
                    return true
                }
                return false
            }


        }, viewLifecycleOwner, androidx.lifecycle.Lifecycle.State.RESUMED)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}