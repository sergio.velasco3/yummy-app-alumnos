package es.estech.yummyapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import es.estech.yummyapp.data.models.recipes.Recipe
import es.estech.yummyapp.databinding.FragmentRecipeDetailBinding
import es.estech.yummyapp.ui.MainActivity
import es.estech.yummyapp.ui.MyViewModel

class RecipeDetailFragment : Fragment() {

    private var _binding: FragmentRecipeDetailBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRecipeDetailBinding.inflate(inflater, container, false)
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (requireActivity() as MainActivity).setToolbar(binding.toolbar)

        super.onViewCreated(view, savedInstanceState)

        //todo obtén aquí la receta seleccionada en la pantalla anterior y llama a fillData para cargar los datos
//        myViewModel...observe { recipe ->
//
//            fillData(recipe)
//        }
    }

    /**
     * Rellena información del fragment con info de la receta
     */
    private fun fillData(recipe: Recipe) {
        Glide.with(this).load(recipe.image).into(binding.vpHeader)
        (requireActivity() as MainActivity).supportActionBar?.title = recipe.name

        with(binding.info) {
            recipe.ingredients?.let {
                var text = ""
                it.forEach {
                    text += "· $it\n"
                }
                tvIngr.text = text
            }
            recipe.instructions?.let {
                var text = ""
                it.forEach {
                    text += "· $it\n\n"
                }
                tvInstruc.text = text
            }

            tvCook.text = "${recipe.prepTimeMinutes} min."
            tvPrep.text = "${recipe.cookTimeMinutes} min."
            tvPerson.text = "${recipe.servings} rat."
            tvRating.text = recipe.rating.toString()
            ratingBar2.rating = recipe.rating?.toFloat() ?: 0F
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}