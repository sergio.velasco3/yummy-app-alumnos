package es.estech.yummyapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import es.estech.yummyapp.data.models.comments.Comment
import es.estech.yummyapp.databinding.FragmentAddCommentBinding
import es.estech.yummyapp.databinding.FragmentCommentsListBinding
import es.estech.yummyapp.ui.MyViewModel
import es.estech.yummyapp.ui.adapters.AdapterComments

/**
 * Fragment con lista de comentarios
 * Funciones añadir, editar y eliminar comentario
 */
class CommentsFragment : Fragment() {

    private lateinit var binding: FragmentCommentsListBinding
    private lateinit var adapter: AdapterComments

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCommentsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // configura recyclerview
        configRecycler()

        //todo llama a la petición descargar comentarios
//        myViewModel...observe {
        //todo llama a adapter.updateList y pásale la lista de Comentarios
//            adapter.updateList(it1)
//        }

        /**
         * click botón añadir
         * muestra un alertdialog con campos y botón de enviar
         * al pulsar el botón enviar, llama a la función sendComment mandando idReceta, idUsuario y comentario
         */
        binding.btnAddComment.setOnClickListener {
            val comBind = FragmentAddCommentBinding.inflate(layoutInflater)
            val builder = AlertDialog.Builder(requireContext()).apply {
                setView(comBind.root)
            }

            val dialog = builder.create()
            dialog.show()

            comBind.btnSend.setOnClickListener {
                val idString = comBind.etId.text.toString()
                val recipeId = comBind.etIdRecipe.text.toString()
                val comment = comBind.etComment.text.toString()

                if (comment.isNotEmpty() && idString.isNotEmpty() && recipeId.isNotEmpty()) {
                    sendComment(recipeId.toInt(), idString.toInt(), comment)
                    dialog.dismiss()
                } else {
                    Toast.makeText(requireContext(), "Hay algún campo vacío", Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    /**
     * Función enviar comentario, con la id de la receta, id del usuario y comentario
     * debes llamar a la función del viewmodel para enviar el comentario
     */
    private fun sendComment(recipeId: Int, userId: Int, body: String) {
        // todo llama a la función para enviar un comentario
    }

    /**
     * inicializa el recyclerview
     */
    private fun configRecycler() {
        val recyclerView = binding.recyclerview

        adapter = AdapterComments(object : AdapterComments.CommentClick {
            override fun onDeleteClick(comment: Comment) {
                // al pulsar en eliminar, llama a la función:
                deleteComment(comment)
            }

            override fun onEditClick(comment: Comment) {
                // al pulsar en editar, llama a la función:
                openEditDialog(comment)
            }
        })

        val layoutManager = LinearLayoutManager(requireContext())

        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }

    /**
     * Función eliminar comentario, recibe como parámetro el comentario
     * debes llamar a la función del viewmodel para eliminar el comentario
     */
    private fun deleteComment(comment: Comment) {
        //todo llama a la función para eliminar un comentario
    }

    /**
     * Función editar comentario, con la id del comentario y comentario editado
     * debes llamar a la función del viewmodel para editar el comentario
     */
    private fun editComment(id: Int, body: String) {
        //todo llama a la función para editar un comentario
    }

    /**
     * Muestra un alertdialog al pulsar en editar comentario. Escribe la info del comentario y permite editarlo.
     * Al pulsar el botón enviar, llama a la función editComment pasando id del comentario y comentario editado
     */
    private fun openEditDialog(comment: Comment) {
        val comBind = FragmentAddCommentBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(requireContext()).apply {
            setView(comBind.root)
        }
        comBind.etComment.setText(comment.body)
        comBind.etId.setText(comment.user?.id.toString())
        comBind.etIdRecipe.setText(comment.postId.toString())
        comBind.etId.isFocusable = false
        comBind.etId.isFocusableInTouchMode = false
        comBind.etIdRecipe.isFocusable = false
        comBind.etIdRecipe.isFocusableInTouchMode = false

        val dialog = builder.create()
        dialog.show()

        comBind.btnSend.setOnClickListener {
            val commentString = comBind.etComment.text.toString()

            if (commentString.isNotEmpty() && comment.id != null) {
                comment.id?.let { it1 -> editComment(it1, commentString) }
                dialog.dismiss()
            } else {
                Toast.makeText(requireContext(), "Hay algún campo vacío", Toast.LENGTH_SHORT).show()
            }
        }
    }
}