package es.estech.yummyapp.data.models.recipes


data class Recipe(
    var id: Int?,
    var name: String?,
    var ingredients: List<String?>?,
    var instructions: List<String?>?,
    var prepTimeMinutes: Int?,
    var cookTimeMinutes: Int?,
    var servings: Int?,
    var difficulty: String?,
    var cuisine: String?,
    var caloriesPerServing: Int?,
    var tags: List<String?>?,
    var userId: Int?,
    var image: String?,
    var rating: Double?,
    var reviewCount: Int?,
    var mealType: List<String?>?
)