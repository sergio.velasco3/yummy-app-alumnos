package es.estech.yummyapp.data.models.comments


import com.google.gson.annotations.SerializedName

data class Comment(
    var id: Int?,
    var body: String?,
    var postId: Int?,
    var user: User?
)