package es.estech.yummyapp.data.models.comments


data class User(
    val id: Int,
    val username: String
)